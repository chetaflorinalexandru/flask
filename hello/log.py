import datetime


class Log:

    @staticmethod
    def write(flag, text):
        try:
            logFile = open("./logs/log_" + datetime.datetime.now().strftime("%Y-%m-%d") + ".txt", "a")
            logFile.write("[%s] %s." % (flag.upper(), text) + "\n")
            logFile.close()
        except IOError as e:
            print(e)
