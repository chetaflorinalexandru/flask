from flask import Flask
from models import Personal, Experience, Education
from flask_swagger_ui import get_swaggerui_blueprint

app = Flask(__name__)

PREFIX = "/api/"

SWAGGER_URL = '/swagger'
API_URL = '/static/swagger.json'
SWAGGERUI_BLUEPRINT = get_swaggerui_blueprint(
    SWAGGER_URL,
    API_URL,
    config={
        'app_name': "hello"
    }
)
app.register_blueprint(SWAGGERUI_BLUEPRINT, url_prefix=SWAGGER_URL)


@app.route(f'{PREFIX}/personal', methods=['GET'])
def personal():
    personal = Personal()
    return personal.get(), 200


@app.route(f'{PREFIX}/personal/<int:profile_id>', methods=['GET'])
def personalById(profile_id):
    personal = Personal()
    return personal.get(id=profile_id), 200


@app.route(f'{PREFIX}/experience', methods=['GET'])
def experience():
    personal = Experience()
    return personal.get(), 200


@app.route(f'{PREFIX}/experience/<position>', methods=['GET'])
def experienceByPosition(position):
    personal = Experience()
    personal.filter(['position', '=', position])
    return personal.get(), 200


@app.route(f'{PREFIX}/education', methods=['GET'])
def education():
    personal = Education()
    return personal.get(), 200


if __name__ == '__main__':
    app.run()
