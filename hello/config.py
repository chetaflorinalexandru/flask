import json
from log import Log


def getDatabase(source):
    try:
        content = open(source, "r")
        return json.load(content)
    except (IOError, json.JSONDecodeError) as e:
        Log.write('error', e)

    return {}


DATABASE = getDatabase('database.json')
