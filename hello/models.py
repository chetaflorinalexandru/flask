import config
from log import Log
from typing import (
    Any,
    List
)


class Model:
    def __init__(self, table):
        self.table = table
        self.filters = []

    def get(self, id=None):
        try:
            if len(self.filters) == 0:
                return config.DATABASE[self.table][id] if id is not None else config.DATABASE[self.table]
            else:
                return self.__applyFilters(config.DATABASE[self.table][id] if id else config.DATABASE[self.table])
        except (KeyError, IndexError):
            Log.write('error', 'Table %s doesn`t exist!' % (self.table))
            return []

    def filter(self, filter_data: List['str'] = {}):
        try:
            if len(filter_data) == 3 and filter_data[1] in ['=', '!=', '>', '<', '>=', '<=']:
                self.filters.append(filter_data)

        except KeyError as e:
            Log.e('error', f'{filter_data} cannot be added')
            print(e)
            raise

    def __applyFilters(self, list):
        query_result = []
        for index, data in enumerate(list):
            for filter in self.filters:
                add = False
                if not data[filter[0]] or not filter[1]:
                    continue

                match filter[1]:
                    case "=":
                        if data[filter[0]] == filter[2]:
                            add = True
                    case "!=":
                        if data[filter[0]] != filter[2]:
                            add = True
                    case ">":
                        if data[filter[0]] > filter[2]:
                            add = True
                    case "<":
                        if data[filter[0]] < filter[2]:
                            add = True
                    case ">=":
                        if data[filter[0]] >= filter[2]:
                            add = True
                    case "<=":
                        if data[filter[0]] <= filter[2]:
                            add = True

                if add:
                    query_result.append(data)
        return query_result


class Personal(Model):
    def __init__(self):
        super().__init__(table="personal_profiles")


class Experience(Model):
    def __init__(self):
        super().__init__(table="experience")


class Education(Model):
    def __init__(self):
        super().__init__(table="education")
