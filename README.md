## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install foobar.

Step:1
```bash
pip install virtualenv
```
Step:2
```bash
python -m venv .venv
```
Step:3

```bash
source .venv/bin/activate #for linux
.venv\Scripts\activate #for windows
```
Step:4
```bash
pip install -r requirements.txt
```
Step:5
```bash
python hello/hello.py
```

## Endpoints


###### Swagger UI url is http://127.0.0.1:5000/swagger/

For personal details the endpoint is :
```
http://127.0.0.1:5000/api/personal
```
and you can call with curl running this command in terminal:
```
curl http://localhost:5000/api/personal
```
For personal details the endpoint by id is :
```
http://127.0.0.1:5000/api/personal/{id}
```
and you can call with curl running this command in terminal:
```
curl http://localhost:5000/api/personal/{id}
```
For experience details the endpoint is :
```
http://127.0.0.1:5000/api/experience
```
and you can call with curl running this command in terminal:
```
curl http://localhost:5000/api/experience
```
For experience details the endpoint by position is :
```
http://127.0.0.1:5000/api/experience/{position}
```
and you can call with curl running this command in terminal:
```
curl http://localhost:5000/api/experience/{position}
```
For education details the endpoint is :
```
http://127.0.0.1:5000/api/education
```
and you can call with curl running this command in terminal:
```
curl http://localhost:5000/api/education
```
